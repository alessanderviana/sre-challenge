# Phoenix App


As Tecnologias utilizadas foram:
 - Docker,
 - GitLab / GitLab CI
 - Kubernetes (GKE)


## Docker
O Docker foi utilizado para "conteinerizar" a aplicação. O Docker dispensa comentários, mas uma de suas vantagens neste caso é flexibilizar o deploy da aplicação, que pode ser feito na forma de um simples contêiner, ou pode-se utilizar um cluster do Docker Swarm ou do Kubernetes.


## GitLab / GitLab CI
O GitLab CI foi utilizado ao invés do Jenkins, pelos seguintes motivos:
 - o projeto está no GitLab,
 - o GitLab CI utiliza a linguagem YAML (mais simples) para a criação de pipelines,
 - o GitLab CI é open source,
 - e o mais importante: o GitLab CI possui integração com a nuvem do Google para criação de clusters do kubernetes.

![gitlab-phoenix-cluster](pictures/gitlab-phoenix-cluster.png)

Uma das vantagens desta integração do GitLab com o Google Kubernetes Engine (GKE) é que, após iniciar / adicionar o cluster, conseguimos instalar o Helm (um tipo de gerenciador de pacotes para o kubernetes) e com este, conseguimos instalar outras aplicações pré definidas como o Prometheus, o GitLab Runner, o Cert-Manager (gerenciador de certificados) e o mais importante, na minha opinião, um Ingress Controller.

Utilizamos também o registry do GitLab, para armazenamento das imagens do Docker.

![gitlab-container-registry](pictures/gitlab-container-registry.png)

## Kubernetes (Google Kubernetes Engine)
O Kubernetes foi a escolha para orquestração de contêiners.
O GKE, sistema do Google para gerenciamento e orquestração de contêiners, foi utilizado por possuir integração com o GitLab, além de fornecer o ambiente kubernetes praticamente pronto para uso, permitindo-nos foco no trabalho principal.

![google-kubernetes-engine](pictures/google-kubernetes-engine.png)

# Visão geral do processo
Para criação do contêiner/imagem utilizamos um Dockerfile com a imagem erlang:21-alpine. A versão 1.8.1 do Elixir já fazia parte da image, instalada via compilação do código fonte. O passo seguinte foi a instalação do framework Phoenix 1.4.3.

A escolha da imagem baseada no Alpine tem a intenção de manter o tamanho das imagens geradas com um mínimo possível.

A opção pela imagem erlang:21-alpine se deu pela facilidade de reutilização, pois já nos fornecia a VM Erlang e o Elixir instalados. O nosso trabalho nesta etapa foi apenas adicionar a esta imagem a instalação do Phoenix Framework.

O pipeline, criado com a ferramenta GitLab CI, possui os estágios:

 1. build - Cria uma imagem com base no Dockerfile que está na raiz do repositório e faz o push para o registry do Gitlab

 2. test - Executa o contêiner no modo de testes, baseado na imagem criada acima, em seguida executa os testes

 3. commit - Altera o contêiner para o modo produção, faz o commit do contêiner (converte-o em imagem) e novamente faz o push para o registry do Gitlab

 4. deploy - Faz o deploy da aplicação para o kubernetes (GKE), criando o deployment, service e ingress. Cria também o cronjob (mix talk).


## Informações do ambiente
 - O cluster criado possui 2 nodes,
 - O deploy cria 2 réplicas da aplicação,
 - O deploy cria 1 serviço do tipo NodePort para a aplicação,
 - O deploy cria um recurso ingress que escuta na porta 80 e redireciona para a porta 4000 (o Ingress Controller foi instalado como aplicação pré definida utilizando o GitLab),
 - O cronjob é um pod baseado na image busybox, que executa o comando especificado, utilizando um dos pods da aplicação, a cada 30 min.

![curl-test-phoenix-app](pictures/curl-test-phoenix-app.png)

![kubectl-production-describe-cron-pod](pictures/kubectl-production-describe-cron-pod.png)
