FROM erlang:21-alpine

LABEL maintainer="Alessander Viana <alessander.viana@hotmail.com>"

# Create the WORKDIR
RUN mkdir /app

# Set the Current Working Directory
WORKDIR /app

# Copy local code into the container WORKDIR
COPY . /app

# Elixir expects utf8, sets ENV to PROD
ENV ELIXIR_VERSION='v1.8.1' \
        LANG=C.UTF-8 \
        MIX_ENV=test

# Install Elixir, install Phoenix and changes the app to PROD mode
RUN set -xe \
        && ELIXIR_DOWNLOAD_URL="https://github.com/elixir-lang/elixir/archive/${ELIXIR_VERSION}.tar.gz" \
        && ELIXIR_DOWNLOAD_SHA256="de8c636ea999392496ccd9a204ccccbc8cb7f417d948fd12692cda2bd02d9822" \
        && buildDeps=' \
                ca-certificates \
                curl \
                make \
        ' \
        && apk add --no-cache --virtual .build-deps $buildDeps \
        && curl -fSL -o elixir-src.tar.gz $ELIXIR_DOWNLOAD_URL \
        && echo "$ELIXIR_DOWNLOAD_SHA256  elixir-src.tar.gz" | sha256sum -c - \
        && mkdir -p /usr/local/src/elixir \
        && tar -xzC /usr/local/src/elixir --strip-components=1 -f elixir-src.tar.gz \
        && rm elixir-src.tar.gz \
        && cd /usr/local/src/elixir \
        && make install clean \
        && apk del .build-deps \
        && cd /app \
        && mix local.hex --force \
        && mix local.rebar --force \
        && mix archive.install --force hex phx_new 1.4.3 \
        && mix deps.get

# Exposes port 4000 to the world
EXPOSE 4000

CMD ["mix", "phx.server"]
