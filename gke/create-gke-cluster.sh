gcloud beta container \
    --project "infra-como-codigo-e-automacao" \
    clusters create "phoenix-cluster" \
    --zone "us-central1-b" \
    --username "admin" \
    --cluster-version "1.11.8-gke.6" \
    --machine-type "n1-standard-1" \
    --image-type "COS" \
    --disk-type "pd-standard" \
    --disk-size "100" \
    --num-nodes "2" \
    --enable-cloud-logging \
    --enable-cloud-monitoring \
    --no-enable-ip-alias \
    --scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" \
    --network "projects/infra-como-codigo-e-automacao/global/networks/default" \
    --subnetwork "projects/infra-como-codigo-e-automacao/regions/us-central1/subnetworks/default" \
    --addons HorizontalPodAutoscaling,HttpLoadBalancing \
    --enable-autoupgrade \
    --enable-autorepair
